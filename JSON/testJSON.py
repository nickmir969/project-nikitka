import random
import json

name = ["Ivan", "Vasiliy", "Roman", "Stepan", "Dmitriy", "Stanislav"]
surname = ["Ivanov", "Vasilyev", "Romanov", "Stepanov", "Dmitriev", "Stanislavov"]
patr = ["Ivanovich", "Vasilevich", "Romanovich", "Stepanovich", "Dmitrievich", "Stanislavovich"]

p = {}
td = {}

print("==================")

max_p = 20
for i in range(max_p):
  td["name"] = name[random.randint(0,5)]
  td["surname"] = surname[random.randint(0,5)]
  td["patr"] = patr[random.randint(0,5)]
  td["age"] = random.randint(20,60)
  p[i+1] = td.copy()

print(json.dumps(p, indent = 4))
