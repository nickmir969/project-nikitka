import requests
import json

key_api = "13d244e904d02a21dd2d334377cf7afb"
url = "http://api.openweathermap.org/data/2.5/weather?"
city = input("Enter the name of city: ")
complete_url = url + "appid=" + key_api + "&q=" + city
resp = requests.get(complete_url)
x = resp.json()
#print(json.dumps(x, indent = 2))
if x["main"] != "404":
  y = x ["main"]
  temp = y["temp"]
  press = y["pressure"]
  hum = y["humidity"]
  z = x["weather"]
  desc = z[0]["description"]
  print("Temperature in Kelvin: " + str(temp))
  print("Atmosphere in GPa: " + str(press))
  print("Humidity in %: " + str(hum))
  print("Description of weather: " + str(desc)) 
else:
  print("City not found")