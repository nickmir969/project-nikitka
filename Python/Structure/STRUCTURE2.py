class vertex:
  def __init__(self,data):
    self.data = data
    self.left = self.right = None

def searchMax(root):
  if (root == None):
    return -999999999999
  research = root.data
  resleft = searchMax(root.left)
  resright = searchMax(root.right)
  if (resleft > research):
    research = resleft
  if (resright > research):
    research = resright
  return research

if __name__ == "__main__":
  root = vertex(2)
  root.left = vertex(7)
  root.right = vertex(5)
  root.left.right = vertex(6)
  root.left.right.left = vertex(1) 
  root.left.right.right = vertex(11)
  root.right.right = vertex(9) 
  root.right.right.left = vertex(4)
  print("Max key in tree this: ", searchMax(root))