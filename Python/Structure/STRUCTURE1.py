import copy

class Tree:
  def __init__(self,data):
    self.data = data
    self.left = self.right = None

def s(node):
  if (node == None):
    return
  else:
    tmp = node
    copy(node.left)
    copy(node.right)
    tmp = node.left
    node.left = node.right
    node.right = tmp

def irr(node):
  if (node == None):
    return
  irr(node.left)
  print(node.data, end = " ")
  irr(node.right)

if __name__ == "__main__":
  source = Tree(input("Type 1 argument: ", ))
  source.left = Tree(input("Type 2 argument: ", ))
  source.right = Tree(input("Type 3 argument: ", ))
  source.left.left = Tree(input("Type 4 argument: ", ))
  source.left.right = Tree(input("Type 5 argument: ", ))
  print("Tree T: ")
  irr(source)
  copy.copy(source)
  print("Copy Tree T1: ")