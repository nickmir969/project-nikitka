lines = []
scenario = {}

with open("test.txt") as file:
    lines = [row.strip() for row in file]

mod = 0
ctr = 0
name = ''

for line in lines:
  if line == 'roles:':
    mod = 1
    continue
  if line == 'textLines:':
    mod = 2
    continue
  if mod == 1:
    scenario[line] = []
  if mod == 2:
    ctr += 1
    newLine = str(ctr) + ')'
    m = 0
    newName = ''
    for l in line:
      if l == ':':
        if newName in scenario.keys():
          name = newName
          m = 1
        continue
      if m == 0:
        newName += l
      if m == 1:
        newLine += l
    if m == 0:
        ctr -= 1
        scenario[name].append(newName)
        continue
    scenario[name].append(newLine)


file = open("output.txt", "w")
for name in scenario.keys():
    file.write(name + ':' + '\n')
    for line in scenario[name]:
        file.write(line + '\n')
    file.write("======================\n")
