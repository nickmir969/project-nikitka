for number in range(1, 10000):
  number_del = [num for num in range(1, number) if number % num == 0]
  number_2 = sum(number_del)
  number_2_del = [num for num in range(1, number_2) if number_2 % num == 0]
  if number == sum(number_2_del) and number_2 == sum(number_del):
    print(number, 'и', number_2, 'дружественные числа')