n = int(input("Enter the number: ", ))
base = int(3)
res = ''

if n == 0:
  print(0)

while n > 0:
  res = str(n % 3) + str(n % 3) + res
  n //= base
  print(int(res, 3))