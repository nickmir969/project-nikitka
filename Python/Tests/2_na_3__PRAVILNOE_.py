n = int(input("Enter the number: ", ))
base = int(3)
res = ''

if n == 0:
  print(0)

else:

  while n > 0:
    res = str(n % 3) + str(n % 3) + res
    n //= base
  
  res = int(res)
  
  new = 0
  d = 0

  while res > 0:
    new = (res % 10 * base**d) + new
    d = d + 1
    res = res // 10
  
  print(new)