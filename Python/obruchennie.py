def res(n):
  for number_1 in range(1, n):
    summ_1 = 1
    i = 2
    while i * i <= number_1:
      if number_1 % i == 0:
        summ_1 = summ_1 + i
        if i*i != number_1:
          summ_1 += number_1 // i
      i = i + 1
    if summ_1 > number_1:
      number_2 = summ_1 - 1
      summ_2 = 1
      j = 2
      while j * j <= number_2:
        if number_2 % j == 0:
          summ_2 += j
          if j*j != number_2:
            summ_2 += number_2 // j
        j = j + 1
      if summ_2 == number_1 + 1:
        print(number_1, 'и', number_2, 'обрученные числа')

n = 10000
res(n)