img = document.getElementById("image");

check = false;

block = document.querySelector('.none');

x1 = 0;
y1 = 0;

x2 = 0;
y2 = 0;

x1Text = document.querySelector('.x');
y1Text = document.querySelector('.y');

x2Text = document.querySelector('.x2');
y2Text = document.querySelector('.y2');

img.onclick = (e) => {
    if (check == false) {
        x1Text.innerHTML = e.offsetX;
        x1 = e.offsetX;
        y1Text.innerHTML = e.offsetY;
        y1 = e.offsetY;
        check = true;
    }
    else if (check == true) {
        x2Text.innerHTML = e.offsetX;
        x2 = e.offsetX;
        y2Text.innerHTML = e.offsetY;
        y2 = e.offsetY;
        check = false;
        var x3 = Math.min(x1,x2); //Smaller X
        var x4 = Math.max(x1,x2); //Larger X
        var y3 = Math.min(y1,y2); //Smaller Y
        var y4 = Math.max(y1,y2); //Larger Y
        block.style.left = x3 + 'px';
        block.style.top = y3 + 'px';
        block.style.width = x4 - x3 + 'px';
        block.style.height = y4 - y3 + 'px';
    }
}
 
function remove() {
    x1 = 0;
    x2 = 0;
    y1 = 0;
    y2 = 0;
    block.style.width = 0;
    block.style.height = 0;
    block.style.top = 0;
    block.style.left = 0;
    x1Text.innerHTML = "";
    x2Text.innerHTML = "";
    y1Text.innerHTML = "";
    y2Text.innerHTML = "";
}

function send() {
    var formData = new FormData();
    formData.append("x1", x1);
    formData.append("x2", x2);
    formData.append("y1", y1);
    formData.append("y2", y2);

    var request = new XMLHttpRequest();
    request.open("POST", "/index.php");
    request.send(formData);
}

function save ()  {
    let f = file1.files[0];
    if (f) {
        image.src = URL.createObjectURL(f);
        localStorage.setItem('myImage', image.src);
    }
}

image.src = localStorage.getItem('myImage')