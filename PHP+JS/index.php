<?php 
  if($_POST)
  {
    $x1 = $_POST['x1'] ?? '';
    $x2 = $_POST['x2'] ?? '';
    $y1 = $_POST['y1'] ?? '';
    $y2 = $_POST['y2'] ?? '';

    $filename = uniqid() . '.json';
    $taskList = json_decode($filename,TRUE);
    unset($filename); 
    $taskList[] = array('X1'=>$x1, 'Y1'=>$y1, 'X2'=>$x2, 'Y2'=>$y2); 
    file_put_contents('C:\data/data.json',json_encode($taskList));
    unset($taskList);  

    if (!file_exists('data'))
    {
        mkdir('data', 0777);
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <link rel="stylesheet" href="style.css" type="text/css" />
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Image</title>
  </head>
  <body>
    <div class="kar">
      <input type="file" id="file1" src="" />
      <button onclick="save()">Save</button>
      <img id="image"/>
      <br>
    <div class="none"></div>
    </div>
    <div class="coords">
      X1: <span class="x"></span><br />
      Y1: <span class="y"></span>
    </div>
    <div class="coords2">
      X2: <span class="x2"></span><br />
      Y2: <span class="y2"></span>
    </div>
    <button onclick="remove()">Очистить</button>
    <button onclick="send()">Отправить</button>
  </body>
  <script src="script.js"></script>
</html>